import { Request, Response } from "express";
import Persona from "../models/persona.model";
import IPersona from "../interfaces/persona.interface";
import bcrypt from "bcrypt";

export const index = async (req: Request, res: Response) => {
  try {
    const { ...data } = req.query;
    let filters = { ...data };

    if (data.nombre) {
      filters = {
        ...filters,
        fullName: { $regex: data.fullName, $options: "i" },
      };
    }

    let personas = await Persona.find(filters);

    res.json(personas);
  } catch (error) {
    res.status(500).send("Algo salio mal");
  }
};

export const show = async (req: Request, res: Response) => {
  const id = req?.params?.id;
  try {
    let persona = await Persona.findById(id);

    if (!persona) {
      res.status(404).send(`No se encontro la persona con id: ${id}`);
    } else {
      res.json(persona);
    }
  } catch (error) {
    res.status(500).send("Algo salio mal");
  }
};

export const create = async (req: Request, res: Response) => {
  try {
    const { ...data } = req.body;

    const persona: IPersona = new Persona({
      fullName: data.fullName,
      email: data.email,
      password: data.password,
      phoneNumber: data.phoneNumber,
      rol: data.rol,
    });

    await persona.save();

    res.status(200).json(persona);
  } catch (error) {
    res.status(500).send("Algo salio mal");
  }
};

export const update = async (req: Request, res: Response) => {
  const id = req?.params?.id;
  const { ...data } = req.body;
  try {
    let persona = await Persona.findById(id);

    if (!persona)
      return res.status(404).send(`No se encontro la persona con id: ${id}`);

    if (data.fullName) persona.fullName = data.fullName;
    if (data.email) persona.email = data.email;
    if (data.password) persona.password = data.password;
    if (data.phoneNumber) persona.phoneNumber = data.phoneNumber;
    if (data.rol) persona.rol = data.rol;

    await persona.save();

    res.status(200).json(persona);
  } catch (error) {
    res.status(500).send("Algo salio mal");
  }
};

export const destroy = async (req: Request, res: Response) => {
  const id = req?.params?.id;

  try {
    let persona = await Persona.findByIdAndDelete(id);
    if (!persona)
      res.status(404).send(`No se encontro la persona con id: ${id}`);
    else res.status(200).json(persona);
  } catch (error) {
    res.status(500).send("Algo salio mal");
  }
};

export const login = async (req: Request, res: Response) => {
  try {
    const { ...data } = req.body;
    let persona = await Persona.findOne({ email: data.email });
    if (!persona) {
      res
        .status(404)
        .send(`No se encontro la persona con el email: ${data.email}`);
    } else {
      const sonIguales = await bcrypt.compare(data.password, persona.password);
      if (sonIguales) {
        res.status(200).json("ok");
      } else {
        res.status(401).json("datos incorrectos");
      }
    }
  } catch (error) {
    res.status(500).send("Algo salio mal");
  }
};
