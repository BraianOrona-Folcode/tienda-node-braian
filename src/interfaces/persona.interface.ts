import { Document } from "mongoose";

export default interface IPersona extends Document {
  _id: string;
  fullName: string;
  email: string;
  password: string;
  phoneNumber: string;
  rol: string;
}
