import { Document } from "mongoose";
import { ObjectId } from "mongodb";

export default interface IVenta extends Document {
  _id: string;
  formaDePago: string;
  precioTotal: number;
  estado: string;
  personaId: ObjectId;
  productos: Array<string>;
  createdAt: Date;
  updatedAt: Date;
}
