import { model, Schema } from "mongoose";
import IPersona from "../interfaces/persona.interface";

const PersonaSchema = new Schema(
  {
    fullName: {
      type: String,
      required: [true, "El nombre es obligatorio"],
    },
    email: {
      type: String,
      required: [true, "El email es obligatorio"],
    },
    password: {
      type: String,
      required: [true, "La contraseña es obligatoria"],
    },
    phoneNumber: {
      type: String,
    },
    rol: {
      type: String,
      required: [
        true,
        'El rol es obligatorio. Valores posibles: "admin" / "cliente"',
      ],
      enum: ["admin", "cliente"],
    },
  },
  {
    timestamps: { createdAt: true, updatedAt: true },
  }
);

export default model<IPersona>("Persona", PersonaSchema);
