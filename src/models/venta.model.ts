import { model, Schema } from "mongoose";
import IVenta from "../interfaces/venta.interface";

const VentaSchema = new Schema(
  {
    formaDePago: {
      type: String,
      default: "contado",
      enum: ["contado", "tarjeta"],
    },
    precioTotal: {
      type: Number,
      default: 0,
    },
    estado: {
      type: String,
      default: "APROBADA",
      enum: ["APROBADA", "ANULADA"],
    },
    personaId: {
      type: Schema.Types.ObjectId,
      ref: "Personas",
      required: true,
    },
    productos: [
      {
        type: Schema.Types.ObjectId,
        ref: "Productos",
      },
    ],
  },
  {
    timestamps: { createdAt: true, updatedAt: true },
  }
);

export default model<IVenta>("Ventas", VentaSchema);
