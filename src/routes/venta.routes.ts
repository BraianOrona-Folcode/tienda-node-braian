import { Router } from "express";
import { body } from "express-validator";
import * as ventaController from "../controllers/venta.controller";
import { validPersonaId } from "../helper/db-validator";
import { validateFields } from "../middlewares/validate-fields";

const router = Router();

router.post(
  "/",
  [body("personaId").custom(validPersonaId), validateFields],
  ventaController.store
);

router.get("/", ventaController.index);
router.get("/:id", ventaController.show);
router.put("/:id", ventaController.update);
router.delete("/:id", ventaController.destroy);
router.post(
  "/:ventaId/productos/:productoId",
  ventaController.facturarProducto
);
router.delete(
  "/:ventaId/productos/:productoId",
  ventaController.reponerProducto
);

export default router;
